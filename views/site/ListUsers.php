<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form ActiveForm */
?>
<div class="ListUsers">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'user_login') ?>
        <?= $form->field($model, 'user_password') ?>
        <?= $form->field($model, 'user_firstname') ?>
        <?= $form->field($model, 'user_lastname') ?>
        <?= $form->field($model, 'user_sex') ?>
        <?= $form->field($model, 'data_create') ?>
        <?= $form->field($model, 'user_email') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- ListUsers -->
