<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to login:</p>
<!--    --><?// if(isset($model)){ ?>
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>


        <?= $form->field($model, 'user_login') ?>
        <?= $form->field($model, 'user_password')->input(['type' => 'number'])?>
        <?= $form->field($model, 'user_firstname')?>
        <?= $form->field($model, 'user_lastname')?>
        <?= $form->field($model, 'user_sex')->dropDownList([
            'men' => 'men',
            'women' => 'women',
            'not sure'=>'not sure'
        ]);?>
        <?= $form->field($model, 'user_email')?>
        <?= $form->field($modelAdress, 'adress_index')?>
        <?= $form->field($modelAdress, 'adress_country')?>
        <?= $form->field($modelAdress, 'adress_city')?>
        <?= $form->field($modelAdress, 'adress_street')?>
        <?= $form->field($modelAdress, 'adress_house_num')?>
        <?= $form->field($modelAdress, 'adress_app_num')?>

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>
<!--    --><?// } ?>
<!--    --><?// if(isset($modelAdress)){ ?>
<!--        --><?php //$form = ActiveForm::begin([
//            'id' => 'adress-form',
//            'layout' => 'horizontal',
//            'fieldConfig' => [
//                'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
//                'labelOptions' => ['class' => 'col-lg-1 control-label'],
//            ],
//        ]); ?>
<!---->
<!--        --><?//= $form->field($modelAdress, 'adress_index')?>
<!--        --><?//= $form->field($modelAdress, 'adress_country')?>
<!--        --><?//= $form->field($modelAdress, 'adress_city')?>
<!--        --><?//= $form->field($modelAdress, 'adress_street')?>
<!--        --><?//= $form->field($modelAdress, 'adress_house_num')?>
<!--        --><?//= $form->field($modelAdress, 'adress_app_num')?>
<!---->
<!---->
<!--        <div class="form-group">-->
<!--            <div class="col-lg-offset-1 col-lg-11">-->
<!--                --><?//= Html::submitButton('Adress', ['class' => 'btn btn-primary', 'name' => 'adress-button']) ?>
<!--            </div>-->
<!--        </div>-->
<!---->
<!--        --><?php //ActiveForm::end(); ?>
<!--   --><?// } ?>

</div>
