-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Ноя 27 2019 г., 09:14
-- Версия сервера: 5.7.24-0ubuntu0.18.04.1
-- Версия PHP: 7.2.11-3+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `yii2bintime`
--

-- --------------------------------------------------------

--
-- Структура таблицы `adress`
--

CREATE TABLE `adress` (
  `adress_id` int(11) NOT NULL,
  `adress_user_id` int(11) NOT NULL,
  `adress_index` varchar(11) NOT NULL,
  `adress_country` varchar(2) NOT NULL,
  `adress_city` varchar(255) NOT NULL,
  `adress_street` varchar(255) NOT NULL,
  `adress_house_num` varchar(255) NOT NULL,
  `adress_app_num` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `adress`
--

INSERT INTO `adress` (`adress_id`, `adress_user_id`, `adress_index`, `adress_country`, `adress_city`, `adress_street`, `adress_house_num`, `adress_app_num`) VALUES
(1, 1, '01253', 'ua', 'kiev', 'Olimp', '123', NULL),
(2, 1, '012531', 'ua', 'kiev', 'Olimp1', '321', NULL),
(3, 1, '0565', 'ua', 'kiev', 'Olimp2', '321', NULL),
(4, 1, '0565', 'ua', 'kiev', 'Olimp2', '321', NULL),
(5, 1, '0565', 'ua', 'kiev', 'Olimp2', '321', NULL),
(6, 1, '0565', 'ua', 'kiev', 'Olimp2', '321', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_login` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_firstname` varchar(255) NOT NULL,
  `user_lastname` varchar(255) NOT NULL,
  `user_sex` varchar(255) NOT NULL,
  `data_create` date NOT NULL,
  `user_email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`user_id`, `user_login`, `user_password`, `user_firstname`, `user_lastname`, `user_sex`, `data_create`, `user_email`) VALUES
(47, 'Login', '$2y$13$gwW8w12SAcp1BOZOtpo6fuQZ4dclHqwLg8i5o9KNyF90zdmYr/np2', 'Firstname', 'Lastname', 'Sex', '2019-11-27', 'Email@gmail.com'),
(48, 'Login1', '$2y$13$6p4WG6kFkG3EO0jnCJjZfulbHI2XC4Y.ZWVaCuATBbeeGL5iXNHby', 'Firstname1', 'Lastname1', 'Sex', '2019-11-27', 'Email1@gmail.com'),
(49, 'Login2', '$2y$13$GUdSOCFBuHgqn864VOFMDekml0SxR84JBAg4NEMfi1WR8fdU0YD6a', 'Firstname2', 'Lastname2', 'Sex', '2019-11-27', 'a@gmail.com'),
(50, 'ghgLogin3', '$2y$13$OkNskPhrlV90QJpTrE7bPO.xpYi3L/eTJldFSUyzLQK5rkLpw6xr2', 'Firstname6', 'Lastname6', 'Sex', '2019-11-27', 'gh@gmail.com'),
(51, 'ghgLogi', '$2y$13$67XfQsGwQ0dDFT55/.AdQu5cacYPgMaoHdzbFbeQE2q.WYWk6UBeu', 'First', 'Last', 'Sex', '2019-11-27', 'jdfkj@gmail.com'),
(52, 'ghgLodsead', '$2y$13$vf15maBqbod6wWo5bnk5nuCz/tap8TecoIxEOV57662qvTzxW/x7m', 'First', 'Last', 'Sex', '2019-11-27', 'dd@gmail.com');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `adress`
--
ALTER TABLE `adress`
  ADD PRIMARY KEY (`adress_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_login` (`user_login`),
  ADD UNIQUE KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `adress`
--
ALTER TABLE `adress`
  MODIFY `adress_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
