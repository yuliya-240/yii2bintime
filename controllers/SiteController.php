<?php

namespace app\controllers;

use Yii;
use yii\debug\models\search\User;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Users;
use app\models\UsersQuery;
use app\models\Adress;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\db\ActiveRecord;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new ActiveDataProvider([
            'query' => Users::find(),
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);
//        $countQuery = clone $model;
//        $pages = new Pagination(['totalCount' => $countQuery->getCount()]);
//        $models = $model->($pages->offset)
//            ->limit($pages->limit)
//            ->all();

//        return $this->render('index', [
//            'models' => $model,
////            'pages' => $pages,
//        ]);
        $this->view->title = 'My Yii Application';
        return $this->render('index.twig', [
        'model' => $model,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new Users();
        $modelAdress = new Adress();
        if ($model->load(Yii::$app->request->post()) && $modelAdress->load(Yii::$app->request->post())) {
            $user = new Users();
            $user->user_firstname=$model->user_firstname;
            $user->user_email=$model->user_email;
            $user->user_login=$model->user_login;
            $user->user_sex=$model->user_sex;
            $user->user_lastname = $model->user_lastname;
            $user->data_create = date("Y-m-d");
            $user->user_password = \Yii::$app->security->generatePasswordHash($model->user_password);
            $user->save();

            if($user->save()) {
//                $rows = (new \yii\db\Query())
//                    ->select('*')
//                    ->from('users')
//                    ->where(['user_email' => $user->user_email])
//                    ->all();
//                print_r($rows->user_id);
                $customer = Users::find()
                    ->where(['user_email' => $user->user_email])
                    ->one();
//                print_r($customer->user_id);
//                die;
                $adress = new Adress();
                $adress->adress_user_id = $customer->user_id;
                $adress->adress_app_num = $modelAdress->adress_app_num;
                $adress->adress_city = $modelAdress->adress_city;
                $adress->adress_country = $modelAdress->adress_country;
                $adress->adress_house_num = $modelAdress->adress_house_num;
                $adress->adress_index = $modelAdress->adress_index;
                $adress->adress_street = $modelAdress->adress_street;
                $adress->save();
                $this->goHome();
            }
//            if($user->save()){
//                $rows = (new \yii\db\Query())
//                    ->select(['user_id'])
//                    ->from('users')
//                    ->where(['user_email' => $user->user_email])
//                    ->one();
//
//                $modelAdress = new Adress();
//                if ($modelAdress->load(Yii::$app->request->post() && $modelAdress->validate())) {
//                    $adress = new Adress();
//                    $adress->adress_user_id=$rows;
//                    $adress->adress_app_num=$modelAdress->adress_app_num;
//                    $adress->adress_city=$modelAdress->adress_city;
//                    $adress->adress_country=$modelAdress->adress_country;
//                    $adress->adress_house_num=$modelAdress->adress_house_num;
//                    $adress->adress_index=$modelAdress->adress_index;
//                    $adress->adress_street=$modelAdress->adress_street;
//
//                }
//                return $this->render('login', [
//                    'modelAdress' =>$modelAdress,
//                    'rows' => $rows,
//                ]);
//
//            }
        }



        $model->user_password = '';
        return $this->render('login', [
            'model' => $model,
            'modelAdress' =>$modelAdress,
        ]);
    }


    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionView($id)
    {
        $user_info=(new \yii\db\Query())
                    ->select('*')
                    ->from('users')
                    ->where(['user_id' => $id])
                    ->one();
//        $modelAdress = new Adress();
//        $modelAdress = new ActiveDataProvider([
//            'query' => Adress::find()->active($id)->all(),
//
//        ]);
        $modelAdress = (new \yii\db\Query())
            ->select('*')
            ->from('adress')
            ->where(['adress_user_id' => $id])
        ->all();
        return $this->render('view.twig', [
            'id'=>$id,
            "user" =>$user_info,
            'modelAdress' =>$modelAdress,
        ]);
    }

    public function actionAdress($user_id){
        $modelAdress = new Adress();
        if ($modelAdress->load(Yii::$app->request->post())) {
            $adress = new Adress();
            $adress->adress_user_id = $user_id;
            $adress->adress_app_num = $modelAdress->adress_app_num;
            $adress->adress_city = $modelAdress->adress_city;
            $adress->adress_country = $modelAdress->adress_country;
            $adress->adress_house_num = $modelAdress->adress_house_num;
            $adress->adress_index = $modelAdress->adress_index;
            $adress->adress_street = $modelAdress->adress_street;
            $adress->save();
            $this->goHome();
        }
        return $this->render('adress.twig', [
            'id'=>$user_id,
            'modelAdress' =>$modelAdress,
        ]);
    }
}
