<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "yii2bintime.users".
 *
 * @property int $user_id
 * @property string $user_login
 * @property string $user_password
 * @property string $user_firstname
 * @property string $user_lastname
 * @property string $user_sex
 * @property string $data_create
 * @property string $user_email
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii2bintime.users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_login', 'user_password', 'user_firstname', 'user_lastname', 'user_sex', 'data_create', 'user_email'], 'required'],
            [['data_create'], 'safe'],
//            [['user_login', 'user_password', 'user_firstname', 'user_lastname', 'user_sex', 'user_email'], 'string', 'max' => 255],
            [['user_login'], 'string', 'min' => 4],
            [['user_firstname'] ,'filter','filter'=>'ucfirst'],
            [['user_lastname'] ,'filter','filter'=>'ucfirst'],
            [['user_login'], 'unique'],
            [['user_email'], 'unique'],
            [['user_login', 'user_password', 'user_firstname', 'user_lastname', 'user_sex', 'data_create', 'user_email'], 'required'],
//            [['data_create'], 'safe'],
//            [['user_login'], 'string', 'min' => 4],
//            [['user_firstname'] ,'filter','filter'=>'mb_strtoupper'],
//            [['user_lastname'] ,'filter','filter'=>'mb_strtoupper'],
//            [['user_password'], 'number', 'min' => 6],
//            [['user_login'], 'unique'],
//            [['user_email'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'user_login' => 'User Login',
            'user_password' => 'User Password',
            'user_firstname' => 'User Firstname',
            'user_lastname' => 'User Lastname',
            'user_sex' => 'User Sex',
            'data_create' => 'Data Create',
            'user_email' => 'User Email',
        ];
    }

    /**
     * {@inheritdoc}
     * @return UsersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UsersQuery(get_called_class());
    }
}
