<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "adress".
 *
 * @property int $adress_id
 * @property int $adress_user_id
 * @property int $adress_index
 * @property string $adress_country
 * @property string $adress_city
 * @property string $adress_street
 * @property string $adress_house_num
 * @property string $adress_app_num
 */
class Adress extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii2bintime.adress';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['adress_index', 'adress_country', 'adress_city', 'adress_street', 'adress_house_num'], 'required'],
            [['adress_index'], 'number'],
            [['adress_country'], 'string', 'max' => 2],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'adress_id' => 'Adress ID',
            'adress_user_id' => 'Adress User ID',
            'adress_index' => 'Adress Index',
            'adress_country' => 'Adress Country',
            'adress_city' => 'Adress City',
            'adress_street' => 'Adress Street',
            'adress_house_num' => 'Adress House Num',
            'adress_app_num' => 'Adress App Num',
        ];
    }

    /**
     * {@inheritdoc}
     * @return AdressQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AdressQuery(get_called_class());
    }
    public function active($id)
    {
        return $this->andWhere('[[adress_user_id]]='.$id.'');
    }
}
