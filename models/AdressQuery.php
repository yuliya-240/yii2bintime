<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Adress]].
 *
 * @see Adress
 */
class AdressQuery extends \yii\db\ActiveQuery
{
    public function active($id)
    {
        return $this->andWhere('[[adress_user_id]]='.$id.'');
    }

    /**
     * {@inheritdoc}
     * @return Adress[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Adress|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
